package main

import (
	"fmt"
	"gotool/date"
	"gotool/string"
)

func main() {
	fmt.Println("hello world", date.Now())
	fmt.Println(string.Contains("abcdefg", "bcd"))
}
